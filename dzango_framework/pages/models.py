# coding: utf-8
from django.db import models
from django.utils import timezone


# TODO: Need in translations

# Abstract Classes

class AbstractMptt(models.Model):
    """Иерархия страниц

    """
    class Meta:
        abstract = True

    left = models.PositiveIntegerField(
        blank=True,
        db_index=True,
    )

    right = models.PositiveIntegerField(
        blank=True,
        db_index=True,
    )

    position = models.PositiveIntegerField(
        blank=True,
        db_index=True,
    )


class AbstractMeta(models.Model):
    """МЕТА данные страницы:

    """

    class Meta:
        abstract = True

    meta_title = models.CharField(
        verbose_name='META заголовок',
        max_length=200,
        blank=True,
    )
    meta_description = models.CharField(
        verbose_name='META описание',
        max_length=200,
        blank=True,
    )


class AbstractPage(models.Model):
    """Базовая страница

        Включает в себя все базовые поля

    """

    class Meta:
        abstract = True

    title = models.CharField(
        verbose_name='Заголовок',
        max_length=200,
        blank=False,
        db_index=True,
    )

    slug = models.SlugField(
        verbose_name='Псевдоним',
        max_length=200,
        db_index=True,
        blank=True,
    )

    # Параметры

    published = models.BooleanField(
        verbose_name='Опубликован',
        default=True,
        db_index=True,
    )

    created_date = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True,
        db_index=True,
    )

    modified_date = models.DateTimeField(
        verbose_name='Дата изменения',
        auto_now=True,
        db_index=True,
    )

    publish_up_date = models.DateTimeField(
        verbose_name='Дата начала публикации',
        db_index=True,
        null=True,
        blank=True,
    )

    publish_down_date = models.DateTimeField(
        verbose_name='Дата окончания публикации',
        db_index=True,
        null=True,
        blank=True,
    )



# Classes

class Mptt(AbstractMptt):
    class Meta:
        verbose_name = 'Структура сайта'
        verbose_name_plural = 'Структура сайта'

    parent = models.ForeignKey(
        verbose_name='Родитель',
        to='Mptt',
        blank=True,
        null=True,
        db_index=True,
        related_name='children',
    )

    def get_name(self):
        if self.as_page:
            return self.as_page.title

    def __str__(self):
        return self.get_name()

    def __unicode__(self):
        return self.get_name()



class Page(AbstractMeta, AbstractPage):
    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'

    content = models.TextField(
        verbose_name='Контент',
        blank=True,  # Может быть пустым
    )

    mptt_parent = models.OneToOneField(
        verbose_name=u'Родитель',
        to='Mptt',
        related_name='as_page_children',
    )

    mptt_object = models.OneToOneField(
        verbose_name=u'Элемент страницы',
        to='Mptt',
        related_name='as_page',
        blank=True,
        null=True,
    )

    def save(self, *args, **kwargs):
        if self.mptt_object is None or self.mptt_object is not None and self.mptt_object.parent is not self.mptt_parent:
            mptt_object = Mptt(left=0, right=0, position=0, parent=self.mptt_parent)
            mptt_object.save()
            self.mptt_object = mptt_object
            super(type(self), self).save(*args, **kwargs)



