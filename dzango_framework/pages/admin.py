from django.contrib import admin

from . import models


# Register your models here.

class DzangoMpttModelAdmin(admin.ModelAdmin):
    ordering = ['position',]
    list_display = [
        '__str__',
    ]

class DzangoPageModelAdmin(admin.ModelAdmin):
    fieldsets = [

        ['Основное', {
            'fields': [
                'title',
                'slug',
                'content',
                'mptt_parent',
            ],
        }],

        ['Изображения', {
            'classes': ['collapse', ],
            'fields': [],
        }],

        ['Публикация', {
            'classes': ['collapse', ],
            'fields': [
                'published',
                'publish_up_date',
                'publish_down_date',
            ],
        }],

        ['Мета', {
            'classes': ['collapse', ],
            'fields': [
                'meta_title',
                'meta_description',
            ],
        }],

    ]

    list_display = [
        'title', 'slug', 'published',
    ]


    prepopulated_fields = {
        "slug": [
            "title",
        ],
    }

admin.site.register(models.Mptt, DzangoMpttModelAdmin)
admin.site.register(models.Page, DzangoPageModelAdmin)
