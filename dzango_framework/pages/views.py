from django.shortcuts import render
from django.views import generic

# Create your views here.


class DzangoPageView(generic.TemplateView):
    template_name = 'dzango/pages/page.html'