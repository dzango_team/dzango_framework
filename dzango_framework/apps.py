from django.apps import AppConfig


class DzangoFrameworkConfig(AppConfig):
    name = 'dzango_framework'
